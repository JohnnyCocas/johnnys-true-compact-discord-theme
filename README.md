Johnny's compact theme for Discord.

It makes all text and items as compact as possible, while maintaining a familiar style to vanilla Discord.